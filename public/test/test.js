QUnit.test('Testing calculateArea function with several sets of inputs', function (assert) { 
    assert.equal(tempConvert("abc"), Nan, 'need to give numbers');
    assert.equal(tempConvert("23"), -5, 'Tested with negative numbers');
    assert.equal(tempConvert("10"), -12.2, 'Tested with negative numbers');
    assert.equal(tempConvert("1"), -17.2, 'Tested with negative numbers');
    
});